'use strict';
(() => {
	const form = document.querySelector(".searchForm");
	const results = document.querySelector(".results");
	const selectYear = document.querySelector(".selectYear");
	const searchButton = document.querySelector('.fa-search');
	//automatically populate year selector dated back 40 years
	let max = new Date().getFullYear();
	let min = max - 40;

	for (var i = max; i >= min; i--) {
		var option = document.createElement("option");
		option.value = i;
		option.innerHTML = i;
		selectYear.appendChild(option);
	}
	///

	//on form submit query api.
	form.addEventListener("submit", e => {
		e.preventDefault();
		results.innerHTML = "";
		queryAPI(e.target.children);
	});

	const queryAPI = (children) => {
		//gets gets query depending on search form submission
		var query = validateQuery(children);
		if (query === false) {
			results.innerHTML = "no search term";
			return false
		}
		fetch(query)
			.then(response => {
				return response.json();
			})
			.then(data => {
				if (data.errors) {
					results.innerHTML = "there was some errors";
					results.innerHTML += data.errors;
				} else {
					populateDom(data.results);
				}
			},Promise.resolve())
			.catch(err => {
				console.log(err);
			});
	};
	//dynamically generates the query string based on form field information.
	const validateQuery = formChildren => {
		let keyword = formChildren.keyword;
		let year = formChildren.year;
		let language = formChildren.language;
		let searchOrDiscover = `search`;
		let queryString = ``;

		//if the search term is empty
		if (keyword.value == "") {
			queryString += `&primary_release_year=${year.value}`;
			searchOrDiscover = `discover`;
		} else {
			let replaced = keyword.value.split(" ").join("+");
			queryString += `&query=${replaced}`;
		}

		if (year.selectedIndex > 0) {
			queryString += `&primary_release_year=${year.value}`;
		}

		if (language.selectedIndex > 0) {
			queryString += `&language=${language.value}`;
		}
		//if all fields are empty return false.
		if (keyword.value == "" && year.selectedIndex < 1 && language.selectedIndex < 1) {
			queryString = ``;
			results.innerHTML = `No results searched for`;
			return false
		}

		let api_key = `1e448e0dfcdbb565f5d329820065b4d2`;
		let url = `https://api.themoviedb.org/3/${searchOrDiscover}/movie?api_key=${api_key}`;
		let query = `${url}${queryString}`;

		return query;
	};

	const populateDom = res => {
		res.forEach(e => {
			let imgPath = `https://image.tmdb.org/t/p/w200`;
			let title = `<h2 class="pt-2">${e.title}</h2>`;
			let overview = `<p>${summarizeString(e.overview)}</p>`;
			let readMore = '';
			let average = `<span class="fa fa-star checked"></span><span>${e.vote_average}</span> / <span>10</span>`

			//lol iadd placeholder image if there is no image location available.
			if (e.poster_path === null) {
				imgPath = `http://www.fillmurray.com/200/300`;
			} else {
				imgPath += e.poster_path;
			}

			fetch(`https://api.themoviedb.org/3/movie/${e.id}/external_ids?api_key=1e448e0dfcdbb565f5d329820065b4d2`)
			.then(response => response.json())
			.then(data => {
				let watched = ``;
				if (data.imdb_id != null) {
					readMore = '<a class="read-more btn" href="#">Read More</a>';
				}
				if(e.id in localStorage) {
					watched = `watched`;
				}
				let output = `
				<div class="col-xs-10 col-sm-6 col-md-4">
					<div class="px-50 result ${watched}">
						<div class="movie-box">
							<img src="${imgPath}" />
							<div class="content px-4 pt-4">
								${title}
								${average}
								${overview}
								<a class="watched btn" data-watch="${e.id}" href="#">WATCH</a>
								${readMore}
							</div>
						</div>
					</div>
				</div>`;
				results.innerHTML += output;
			},Promise.resolve())
			.catch(err => {
				console.log(err);
			});
		});
	};
	//summarise Overview for Search Results
	const summarizeString = (str) => {
		if (str.length <= 0) {
		  return '';
		}
		let limit = 200;
		str = str.substring(0, limit);
		str = str.replace(/\s+$/, ""); // right trim
		return str;
	  }

	results.addEventListener('click', e => {
		e.preventDefault();
		if (e.target.classList.contains('watched')) {
			let target = e.target.getAttribute('data-watch');
			localStorage.setItem(target, target);
		}
	});

	//navigation hide show search/navigation.
	let formContainer = document.querySelector('.form-container');
	let navBar = document.querySelector('.navbar');
	let close = document.querySelector('.hideForm');

	searchButton.addEventListener('click', e => {
		e.preventDefault();
		formContainer.classList.add('show');
		navBar.classList.add('hide');
	});

	close.addEventListener('click', e => {
		e.preventDefault();
		formContainer.classList.remove('show');
		navBar.classList.remove('hide');
	});


})();
