"use strict";

const gulp = require("gulp");
const sass = require("gulp-sass");
const browserSync = require("browser-sync");
const minify = require("gulp-minify");
const autoprefixer = require("gulp-autoprefixer");
const sourcemaps = require('gulp-sourcemaps');
const util = require('gulp-util');
const clean = require('gulp-rimraf');

gulp.task("browser-sync", () => {
	browserSync.init(
		["./src/scss/**/*.scss", "./src/scripts/*.js", "./src/**/*.html"],
		{
			server: {
				baseDir: "./dist/"
			}
		}
	);
});

gulp.task('clean', [], function() {
	console.log("Clean all files in build folder");
	return gulp.src("dist/*", { read: false }).pipe(clean());
  });

const onlyRunInDev = (attr) => {
	return !util.env.production ? attr : util.noop()
}

gulp.task("sass", () => {
	return gulp
		.src("./src/scss/*.scss")
		.pipe(onlyRunInDev(sourcemaps.init()))
			.pipe(sass().on("error", sass.logError))
			.pipe(autoprefixer({
				browsers: ['last 2 versions'],
				cascade: false
			}))
		.pipe(onlyRunInDev(sourcemaps.write('.')))
		.pipe(gulp.dest("./dist"));
});

gulp.task("html", () => {
	return gulp.src("./src/*.html").pipe(gulp.dest("./dist/"));
});

gulp.task("scripts", () => {
	return gulp.src("./src/scripts/*.js")
	.pipe(minify({noSource: true}))
	.pipe(gulp.dest("./dist"));
});

gulp.task("default", ["browser-sync", "clean",  "sass", "html", "scripts"], () => {
	gulp.watch("./src/scss/*.scss", ["sass"]);
	gulp.watch("./src/scripts/*.js", ["scripts"]);
	gulp.watch("./src/**/*.html", ["html"]);
	setTimeout(() => {
		browserSync.reload();
	}, 200);
});