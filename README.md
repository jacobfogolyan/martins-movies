# Getting Started

This application assumes that Node.js, NPM ,Gulp and the latest browser versions are installed locally on your machiene.

1. cd to project folder
2. npm install
3. run `gulp` for development environemnt
3. run `gulp --production` for deliverables

## Technology Stack

- Mac
- Visual Studio Code
- NPM
- Node.js
- Gulp.js
- Bootstrap 4
- SASS
- Font Awesome
- Vanilla Javascript ES6 +

> I am aware of the limitations ES6 plus can have on older browsers. However this project is a demonstration of my technical knowledge.

## Instructions

Type in search Term or
Select Year or
Select Language or
all of the above.

Click Watch which greys out movie as "watched" on reload.

## Additions

- Gulp Autoprefixer for browser Support.
- Gulp Sourcemaps for SCSS
- stylesheet tidyup
- reorganised SCSS
- added js comments
- added summerize string function
- added click to show search
- added click to hide search
- added clean task to clean dist folder.
- added environment detection with gulp-util to only run sourcemaps in dev mode.
